# This script analyzes the content of pdf files in a parent folder, recursively.
# This script is dependent upon Ghostscript interpreter binary, that must be present on the defined folder
# If the "$bRemoveProtection" switch is enabled  or $true the script will attempt to convert the file
# to an unprotected PDF, prepending one or more underscores ("_") to the original filename.
# Tested with Ghostscript version 10.4.0 APGL

# Note to self: first execution, over 16800 files took 2 hours and 15 minutes for Analyze only.
# Note to self: removal of protection over 80 files takes 3 minutes -> 16800 files shoud take 10+ hours



param (
  [string]$sGSPath = "E:\GS\bin", # Change to match the location of Ghostscript bin folder
  [string]$sPDFParentFolder = "E:\GS\Test", # Change ot match the parent folder of PDFs to process
  [string]$sLogFolder = "E:\log\", #Change ot match the folder where the log will be written (needs write permissions!)
  [string]$sFilePattern = "*", #File pattern to match. It is not necessary to specify ".pdf" 
  [bool]$bOverWrite = $false, # if true bOverWrites the original file, otherwise it creates a copy of the original file on the same folder, but prefixing it with an underscore
  [bool]$bRemoveProtection = $true # $true enables the protection removal
  )

### Validating parameters

# Check Ghostscript version in the $sGSPath
if ([System.IO.File]::Exists("$($sGSPath)\gswin64c.exe")){
    $sDetectedGSVersion = [System.Diagnostics.FileVersionInfo]::GetVersionInfo("$($sGSPath)\gswin64c.exe") 
    "Using Ghostscript version $($sDetectedGSVersion.ProductVersion)"
} else {
   "Ghostscript not detected. Exiting script." | out-host
   Exit 1
}

# Check sPDFParentFolder
if (-not([System.IO.Directory]::Exists($sPDFParentFolder))){
    "PDF parent folder not found. Exiting script." | out-host
    Exit 2
}

# Check sLogFolder
if (-not([System.IO.Directory]::Exists($sLogFolder))){
    "Log folder not found. Exiting script." | out-host
    Exit 5
}

# Adding GhostScript path to system path if not already there
if (-not ($sGSPath -in $($Env:path).Split([IO.Path]::PathSeparator))){
    $Env:Path += [IO.Path]::PathSeparator + $sGSPath    
}

# Setting the log output file
$sOutFile = "$($sLogFolder)PDF-analysis.txt"

# Function to check if the PDF file is protected, by checking if the Ghostscript output returns a specific "Encryption" string in the output
# Requires a filename to process. The caller shall ensure the file is a pdf.
# Returns true if the file is protected, returns false otherwise
function Test-Protected{
    param(
        [string]$sFileName
    )

    # Preparing temporary file to hold the process standard output
    $sStdOutFile = [System.IO.Path]::GetTempFileName()

    # Running the process to check the file using Ghostscript
    $oProcess = Start-Process -FilePath "gswin64c.exe" -ArgumentList "-dBATCH -dNOPAUSE -dNOPROMPT -dSHORTERRORS -sNODISPLAY -o null -sDEVICE=nullpage $sFileName" -RedirectStandardOut $sStdOutFile -NoNewWindow -PassThru
    $oProcess.WaitForExit()
    
    # Passing the content of the file into a variable
    $sStdOut = get-content $sStdOutFile
    
    # Removing the file
    remove-item $sStdOutFile
    
    #Checking for key string "Encryption"
    if ($sStdOut -match "Encryption"){
      return $true
    } else {
      return $false
    }
}


# Function to Remove the protection of the PDF file. 
# Requires a filename and a switch to tell that the original file shall be overwritten.
# Returns the filename where the unprotected PDF was written
function Remove-Protection{
    param (
        [string]$sFilePath,
        [bool]$bOverWrite=$false
    )

    $oFile = Get-Item -path "$($sFilePath)" 
    
    #Preparing error buffer
    $sStdErrFile = [System.IO.Path]::GetTempFileName()

    # looking for a filename that doesn't exist in order to avoid bOverWrite
    do {
        $sPrefix += "_"
        $sFileName = "$($oFile.Directory.FullName)\$($sPrefix)$($oFile.Name)"
    } while ([System.IO.File]::Exists($sFileName))
       
    $oProcess = start-process -FilePath "gswin64c.exe" -ArgumentList "-dSAFER -dBATCH -dNOPAUSE -dNOPROMPT -dSHORTERRORS -sDEVICE=pdfwrite -sPDFPASSWORD= -sOutputFile=""$($sFileName)"" -dPDFSETTINGS=/prepress -f ""$($sFilePath)""" -RedirectStandardErr $sStdErrFile -NoNewWindow -PassThru
    $oProcess.WaitForExit()

    # Passing the content of the file into a variable
    $sStdErr = get-content $sStdErrFile
    
    # Removing the file
    remove-item $sStdErrFile

    if ($sStdErr -match "Unrecoverable error"){
        "Error while removing protection from $($sFileName): $($sStdErr)" | out-host
        return $null
    }

    if ($bOverWrite){
        # Remove protected file
        Remove-Item -Path $sFilePath
        # Renaming new unprotected file as the old protected one
        rename-item -path $sFileName -newName $sFilePath -Force
        #returning actual filename after renaming
        return $sFilePath
    } else {
      #returning actual new filename
      return $sFileName
    }
}

$iProcessed = 0
$iProtectedFound = 0
$iFailed = 0
$iRemovedProtection = 0

# Listing all files inside the PDF parent folder. Processes all PDFs that match the given pattern
get-childitem $sPDFParentFolder -File -Recurse -Force -Filter "$($sFilePattern)*.pdf" | foreach-object {
    $iProcessed++
    "`nProcessing file $($_.fullname)..." | out-host
    # Checks if the file is protected
    if (Test-Protected -sFileName $_.FullName){
        "File $($_.Fullname) is protected" | out-host
        $iProtectedFound ++
        # Removes protection
        if ($bRemoveProtection){
            "Removing protection from file $($_.fullname)..." | out-host

            # unprotecting the file and writing the unprotected filename to a variable
            $sCurrentFileName =  $(Remove-Protection -sFilePath $_.fullname -bOverWrite $bOverWrite)
            if ($sCurrentFileName -eq $null){
                """Processing failed for $($_.FullName)"",""$($_.CreationTimeUtc)"",""Protected""" | Out-File $sOutFile -Append
                $iFailed++ 
                return              
            }
            # Re-cheking the newly unprotected file
            "Action completed, checking the unprotected file $sCurrentFileName..." | out-host
            
            # Writes to log the result depending on the protection check result.
            if (Test-Protected -sFileName $sCurrentFileName){
               "File $sCurrentFileName is still protected." | out-host
                """Failed protection removal on $sCurrentFileName"",""$($_.CreationTimeUtc)"",""Protected""" | Out-File $sOutFile -Append
            } else {
                "File $sCurrentFileName is no longer protected." | out-host
                """Successful protection removal on $sCurrentFileName"",""$($_.CreationTimeUtc)"",""Unprotected""" | Out-File $sOutFile -Append
                $iRemovedProtection++
            }
        } else {
            """Found file $($_.FullName)"",""$($_.CreationTimeUtc)"",""Protected""" | Out-File $sOutFile -Append
        }
    } else {
        "File $($_.fullname) is not protected" | out-host
        """Found file $($_.FullName)"",""$($_.CreationTimeUtc)"",""Unprotected""" | Out-File $sOutFile -Append
    }
}

"`n*** Execution complete ***`n`
Processed files: $iProcessed`
Protected files: $iProtectedFound`
Errors: $iFailed`
Removed protections: $iRemovedProtection" | out-host


