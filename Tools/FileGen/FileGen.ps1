<#
.SYNOPSIS
    Generates empty files and folders in order to simulate a working folder with files to experiment and test your script with.

.DESCRIPTION
    The script generates empty files with given extension. If date and time awareness is disabled it generates files with a progressive numbering, otherwise it creates evenly distributed files through the given nDays. It can enclose different days in different folders is required. In the date time awareness mode  Creation, LastAccess and LastModified times are set accordingly.
        
    Created in 2023 by Michele Brami (aka 77nn)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

     This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

.EXAMPLE
    . 'FileGen.ps1' -rootFolder "FileGen" -startDate 2023-02-17 -nFiles 4 -nDays 5 -dayFolders
    Creates 5 folders, one for each day starting from 2023-02-17, with 4 files each, dated from midnight every 6 hours. Does not write files if the rootFolder is not empty.
    
.EXAMPLE
    . 'FileGen.ps1' -rootFolder "FileGen" -nFiles 500 -noDateTime -overWrite
    Creates 500 files with progressive names, overwriting them if already present.

.EXAMPLE
    . 'FileGen.ps1' -rootFolder "FileGen" -nFiles 500 
    Creates 500 files on today's date at different times. Does not write files if the rootFolder is not empty.

.NOTES
    Powershell 7.3.1

.LINK
    https://codeberg.org/77nn/PowershellTools

.OUTPUT
    Parameter validation errors do not return an exit code.
    Exit codes:
    0    - Execution successful
    901  - Invalid file extension selected for files
    902  - Calculated file length exceeds 255 characters
    903  - Not creating files that can overwrite or mixup with other files (use -overWrite switch if desired)

#>
[CmdletBinding(SupportsShouldProcess)]
param(
    #Folder where the files will be created into, if the folder does not exist it will be created. Defaults to current location.
    [Alias("rootFolder")]
    [Parameter(Mandatory=$false)]
    [ValidateScript({Test-Path -LiteralPath "$_" -IsValid})] 
    [string]$sRootFolder = $(Get-Location).Path,

    #File extension to add to created files
    [Alias("fileExt")]
    [Parameter(Mandatory=$false)]
    [string]$sFileExt = "txt",

    #Number of files to be created for every day. CreationTime, lastAccessTime and LastWriteTime will be set accordingly. If more than one file is created for every day, times will be spread uniformly across the day, otherwise time defaults to 00:00:01.
    [Alias("nFiles")]
    [Parameter(Mandatory=$false)]
    [ValidateScript({$_ -ge 1})] 
    [int]$iFilesPerDay = 1,

    #Number of days for which files are created. The total number of created files will be iDays * iFilesPerDay.
    [Alias("nDays")]
    [Parameter(Mandatory=$false)]
    [ValidateScript({$_ -ge 1})] 
    [int]$iDays = 1,

    #Date from which the daily files are created onwards. Enter the date as YYYY-MM-DD.
    [Alias("startDate")]
    [Parameter(Mandatory=$false)]
    [ValidateScript({$_ -is [DateTime]})] 
    [datetime]$dtStartingDate = $(Get-Date -date (get-date) -hour 0 -Minute 0 -Second 1),
    
    #Switch that disables date and time awareness if present. All files will be created plainly and named with a progressive number
    [Alias("noDateTime")]
    [switch]$bDateTimeUnAware,

    #Switch that puts files into daily folders that will be created accordingly, if present
    [Alias("dayFolders")]
    [switch]$bDailyFolders,

    #Switch that enables overwriting files with same names, if present. If this switch is not present files will not be created in case the folder is not empty.
    [Alias("overWrite")]
    [switch]$bOverWrite
)

#Checking inputs

#sRootFolder

#Checking if sRootFolder does not exist or it is not a container type
if (-not(Test-Path -LiteralPath "$sRootFolder" -PathType Container)){
    #Creating sRootFolder is ok
    "$sRootFolder will be created." | out-host
} else {
    # sRootFolder already exists
     ("$sRootFolder already exists. " + $(if($bOverWrite.IsPresent){"Existing files will be overwritten."}else{"Existing files will NOT be overwritten."})) | out-host
}

#sFileExt

#Checking if sFileExt contains valid characters for a file path
if (-not(Test-Path -Path "$sRootFolder\file.$sFileExt" -IsValid)){
    "$sFileExt extension seems to induce the creation of an invalid path. The execution cannot continue." | out-host
    Exit 901 #Invalid file extension
}

#Checking file path length

#Expecting that the the sum of lengths for sRootFolder + sFileExt + typical file name does not exceed 255 chars.
#sRootFolder could also be a relative path to currentfolder. Need to check this out as well.
#Typical file name is dynamically created as a formatted datetime "YYYY-MM-DD_hh-mm-ss" that amounts to 19+1 chars.
#Typical daily folder name is dynamically created as a formatted date "YYYY-MM-DD" that amounts to 10+1 chars

$iPathLength = $(if ([System.IO.Path]::IsPathRooted($sRootFolder)){$sRootFolder.Length} else {$sRootFolder.Length + (Get-Location).Path.Length }) +
               1                         +    #"\" 
               $(if ($bDailyFolders){11}) +    # daily folder + "\"
               21                        +    # file name + "." 
               $sFileExt.Length               # ext

if ($iPathLength -gt 255){
    "The files length exceeds 255 characters. The execution cannot continue." | out-host
    Exit 902 #Maximum path length exceeded
}


#Creating root folder if it doesn't exist
if (-not(Test-Path -LiteralPath "$sRootFolder" -PathType Container)){
    $oRootFolder = New-Item "$sRootFolder" -ItemType Directory
    "$($oRootFolder.FullName) folder was created" | out-host
} else {
    $oRootFolder = Get-Item -Path "$sRootFolder"
}

#Moving to RootFolder
"Moving to $($oRootFolder.FullName) folder" | out-host
Set-location "$($oRootFolder.FullName)"

#If datetime awareness is enabled
if (-not $bDateTimeUnAware.IsPresent){

    #setting time difference between files in order to have them equally distributed
    $dtSpan = New-TimeSpan -Seconds (24*60*60/$iFilesPerDay)
    
    #cycle through days
    for ($i = 0; $i -lt $iDays; $i++){
        
        #need to make this a datum so that processing time is out of the way when setting creation 
        $dtDatum = get-date ($dtStartingDate.AddDays($i)) -Second 1
        
        #creating daily folders only if required
        if ($bDailyFolders.IsPresent){
            $sThisFolderName = get-date $dtDatum -format "yyyy-MM-dd"
            $oDailyFolder = New-Item "$sThisFolderName" -ItemType Directory -Force
            "$($oDailyFolder.FullName) folder created" | out-host
            $oDailyFOlder | ForEach-Object {
                $_.LastWriteTime = $dtDatum
                $_.CreationTime = $dtDatum
                $_.LastAccessTime = $dtDatum
            }
            
            
            #Moving to newly created folder
            "Moving to $($oDailyFolder.FullName) folder" | out-host
            Set-Location "$($oDailyFolder.FullName)"
        }
        
        #checking if the folder is actually empty (used later for no-overwrite mode)
        $bIsEmptyFolder = $(Get-childitem -recurse -force).count -eq 0

        #start creating files
        for ($j = 0; $j -lt $iFilesPerDay; $j++){
            
            #Skipping the first file for the day, it will get 00:00:01 as time
            if ($j -gt 0){
                $dtDatum = $dtDatum.AddSeconds($dtSpan.TotalSeconds)
            }
            #preparing filename
            $sThisFileName = get-date $dtDatum -format "yyyy-MM-dd_HH-mm-ss"

            #if overwrite is activated
            if ($bOverWrite.IsPresent){

                #force creation of file
                $oThisFile = (new-item ($sThisFileName+"."+$sFileExt) -ItemType File -Force | ForEach-Object {
                    $_.LastWriteTime = $dtDatum
                    $_.CreationTime = $dtDatum
                    $_.LastAccessTime = $dtDatum
                })
                "$($oThisFile) file created." | out-host
            
            #otherwise creates the file only if the folder we are in is empty
            } elseif ($bIsEmptyFolder){
                $oThisFile = (new-item ($sThisFileName+"."+$sFileExt) -ItemType File | ForEach-Object {
                    $_.LastWriteTime = $dtDatum
                    $_.CreationTime = $dtDatum
                    $_.LastAccessTime = $dtDatum
                })
                "$($oThisFile) file created." | out-host
            } else {
                "Files cannot be created on this folder as it is not empty. Try activating the -overWrite switch to force file creation in this folder" | out-host
                "Execution will be terminated" | Out-Host
                Exit 904                     
            }
        } #daily files cycle completed, moving back to rootFolder
    "Moving to $($oRootFolder.FullName) folder" | out-host
    set-location "$($oRootFolder.FullName)"
    }

#If datetime awareness is disabled   
} else {
    #checking if the folder is actually empty (used later for no-overwrite mode)
    $bIsEmptyFolder = $(Get-childitem -recurse -force).count -eq 0
    #Ignoring date and time awareness and number of days 
    for ($j = 0; $j -lt $iFilesPerDay; $j++){

        #preparing filename
        $sThisFileName = $([string]$j).PadLeft(19,"0")

        #if overwrite is enabled
        if ($bOverWrite.IsPresent){

            #forcing file creation
            $oThisFile = (new-item ($sThisFileName+"."+$sFileExt) -ItemType File -Force)
            "$($oThisFile) file created." | out-host
        
        #otherwise creates files only if the folder is empty
        } elseif ($bIsEmptyFolder){
            $oThisFile = (new-item ($sThisFileName+"."+$sFileExt) -ItemType File -Force)
            "$($oThisFile) file created." | out-host
        } else {
            "Files cannot be created on this folder as it is not empty. Try activating the -overWrite switch to force file creation in this folder" | out-host
            "Execution will be terminated" | Out-Host
            Exit 903 #No vverwriting or mixing up files                    
        }
    }
    #moving back to rootfolder
    "Moving to $($oRootFolder.FullName) folder" | out-host
    set-location "$($oRootFolder.FullName)"
}

"Execution completed, no errors detected" | Out-Host
exit 0