# PowershellTools

Powershell tools is a collection of Powershell script written mainly as exercise in order to experiment features and code writing techniques. The tools should work just fine, but I don't take any responsibility if anythig should fail and cause any damage.

The tools are on the /Tools folder.

Actual tools:

FileGen.ps1
: Creates a set of empty files with different scenarios in order to have a repeatable test set for other scripts testing

PDF-Protection-Remove.ps1
: Scans recusvively pdf files to check if they're protected. May remove protection. Requires Ghostscript.
