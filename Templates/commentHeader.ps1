<#
.SYNOPSIS
    A summary of how the script works and how to use it.

.DESCRIPTION
    A long description of how the script works and how to use it.
    License information:

    Created on 2023 by Michele Brami (aka 77nn)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

     This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

.EXAMPLE
    Sample script usage with description

.NOTES
    Information about the environment, things to need to be consider and other information.

.COMPONENT
    Information about PowerShell Modules to be required.

.LINK
    Useful Link to resources or others.

#>